<?php

namespace App\Controller;

use App\Entity\Wish;
use App\Form\WishType;
use App\Repository\WishRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
#[Route('/wish', name: 'wish_')]
class WishController extends AbstractController
{
    #[Route('/', name: 'list')]
    public function list(WishRepository $repo): Response
    {
        //$wishes= $repo->findBy([],["dateCreated"=>"DESC"]);

        $wishes=$repo->sortedByDate();
        //$wishes = ["Manger des frites au canada", "aller à la fête de la bière","Aller au Wacken"];
        return $this->render('wish/list.html.twig',
        ['wishes'=>$wishes]);
    }

    #[Route('/detail/{id}', name: 'detail')]
    public function detail($id,WishRepository $repo): Response
    {
        $wish=$repo->find($id);
        return $this->render('wish/detail.html.twig', [
            'id' => $id,
            'wish' => $wish
        ]);
    }


    #[Route('/edit/{id}', name: 'edit')]
    public function edit(int $id,WishRepository $repo, Request $request): Response
    {
        $wish = $repo->find($id);

        $wishForm = $this->createForm(WishType::class, $wish);
        $wishForm->handleRequest($request);

        //traitement du formulaire
        if($wishForm->isSubmitted() && $wishForm->isValid()){

            $repo->add($wish, true);
            $this->addFlash("success", "Voeu ajouté avec succès !");
            return $this->redirectToRoute("wish_detail", ["id" => $wish->getId()]);
        }

        return $this->render('wish/add.html.twig', [
            'wishForm' => $wishForm->createView()
        ]);
    }

    #[Route('/add', name: 'add')]
    public function add(WishRepository $repo, Request $request): Response
    {
      //création d'un formulaire
        //création d'un nouveau voeu
        $wish = new Wish();
        $wish
            ->setDateCreated(new \DateTime())
            ->setIsPublished(true)
            ->setAuthor($this->getUser()->getUserIdentifier());

        $wishForm = $this->createForm(WishType::class, $wish);
        $wishForm->handleRequest($request);

        //traitement du formulaire
        if($wishForm->isSubmitted() && $wishForm->isValid()){

            $repo->add($wish, true);
            $this->addFlash("success", "Voeu ajouté avec succès !");
            return $this->redirectToRoute("wish_detail", ["id" => $wish->getId()]);
        }

        return $this->render('wish/add.html.twig', [
            'wishForm' => $wishForm->createView()
        ]);
    }
}
